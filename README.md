# graphql-schema-rubymotion

## Installation

    git clone https://bitbucket.org/mswinson-lib/graphql-schema-rubymotion  

## Prequisites

    docker
    docker-compose

## Usage

setup  

    docker-compose build build

    export S3_BUCKET_REPO=<mybucket>
    export S3_BUCKET_PREFIX=<mybucket_prefix>
    export AWS_DEFAULT_REGION=<aws_region>

build  

    docker-compose run build  

    > make build


deploy

    docker-compose run build

    > make deploy
  


