type Api {
	repository: Repository
}

type Gem {
	name: String
	gem_name: String
	description: String
	url: String
	github: String
	info: String
}

type Query {
	rubymotion: Api
}

type Repository {
	gems(name: String): [Gem]
}

schema {
	query: Query
}
